


class Person{
	constructor(name,birthday,age,skill){
		this.name = name,
		this.birthday = birthday,
		this.age = age,
		this.skill = skill	
	}
}
let skillSet = ['drawing','programming'];
let person1 = new Person('Alex','September', 20, skillSet);

const introduce = (person) =>{
	let {name, birthday, age, skill} = person;
	console.log(`My name is ${person.name}. My birth month is ${person.birthday} and I'm ${person.age} yrs old`);
	console.log(`Skills:`);
	for(let index = 0; index < person.skill.length; index++){
		console.log(`${index+1}.${person.skill[index]}`);
	}
} 

introduce(person1);
class Dog{
	constructor(name, breed, age){
		this.name = name,
		this.breed = breed,
		this.age = age * 15
	}
}

let shitzu = new Dog("Starry","Shitzu",6);
let shiranian = new Dog("Toffee","Shiranian",3);
console.log(shitzu);
console.log(shiranian);